Sub CodeFormat()
   If Selection.range.Start = Selection.range.End Then
      MsgBox "You havn't selected any text"
      Exit Sub
   End If
   
   Dim i As Long
   Dim j As Long
   Dim PrimaryList
   Dim SecondaryList
   
   PrimaryList = Array("for", "to", "do", "while", "if", "else")
   
   For i = 0 To UBound(PrimaryList)
   
      With Selection.Find
           .Text = PrimaryList(i)
           .Replacement.Text = ""
           .Replacement.Font.Color = RGB(79, 129, 189)
           .Forward = True
           .Wrap = wdFindStop
           .MatchWholeWord = True
      End With
      Selection.Find.Execute Replace:=wdReplaceAll
   Next
   
   SecondaryList = Array("<-", "int", "float", "double", "and", "or", "return")
   
   For j = 0 To UBound(SecondaryList)
   
      With Selection.Find
           .Text = SecondaryList(j)
           .Replacement.Text = ""
           .Replacement.Font.Color = RGB(128, 100, 162)
           .Forward = True
           .Wrap = wdFindStop
           .MatchWholeWord = True
      End With
      Selection.Find.Execute Replace:=wdReplaceAll
   Next
End Sub
